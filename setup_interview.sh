echo "Enter email address of candidate"
read email
echo "Creating Interview org for $email"
sfdx force:org:create -f config/project-scratch-def.json -a $email
sfdx force:org:display -u $email --verbose --json >$email_authFile.json
echo "Preparing org for $email"
sfdx force:source:push -u $email
sfdx force:user:permset:assign -n recipes -u $email
sfdx force:data:tree:import -p ./data/data-plan.json -u $email
git fetch -v
git checkout -b interview/$email
git add . && git commit -m "added interview branch for $email"
git push origin interview/$email
